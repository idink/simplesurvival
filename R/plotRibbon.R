plotRibbon <- function(
    x, y, ymin, ymax, xlab = 'x', ylab = 'y', title = NULL,
    line.colour = 'black', ribbon.fill = 'red', ribbon.alpha = 0.3) {
    the.plot <- ggplot2::ggplot() +
        ggplot2::geom_ribbon(
            mapping = ggplot2::aes(x = x, ymin = ymin, ymax = ymax),
            fill = ribbon.fill, alpha = ribbon.alpha
        ) +
        ggplot2::geom_line(
            mapping = ggplot2::aes(x = x, y = y),
            colour = line.colour
        ) +
        ggplot2::xlab(xlab) + ggplot2::ylab(ylab)
    if(!is.null(title)) {
        the.plot <- the.plot + ggplot2::ggtitle(label = title) +
            ggplot2::theme(plot.title = ggplot2::element_text(hjust = 0.5))
    }

    return(the.plot)
}
